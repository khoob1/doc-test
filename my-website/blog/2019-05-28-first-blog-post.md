---
slug: first-blog-post
title: First Blog Post
authors:
  name: Gao Wei
  title: Docusaurus Core Team
  url: https://github.com/wgao19
  image_url: https://github.com/wgao19.png
tags:
  - hola
  - docusaurus
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet

edited by Minh

![](/img/uploads/44891418625_2056c2bf9f_o.png)

Change by Bryant published directly

Added plantuml:

```plantuml
Bob -> Alice : hello
```

```plantuml
class SimplePlantUMLPlugin {
    + transform(syntaxTree: AST): AST
}
```

![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLWZEp2t8IGt8ISmh2VNr2SWfJS_CKwZcKW02ROMIeiIyuhJyeboDuigyaiIAaABKL2i5n-0GJJ39LSlba9gN0ZGC0000)

Test by bryant

Test 1 by bryant

Test2 by bryant