FROM nginx:alpine

ADD nginx.conf /etc/nginx/conf.d/default.conf

ADD my-website/build /var/www/my-website/build/

# for local dev
CMD ["nginx", "-g", "daemon off;"]